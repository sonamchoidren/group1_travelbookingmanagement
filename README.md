## Getting Started

Welcome to the VS Code Java world. Here is a guideline to help you get started to write Java code in Visual Studio Code.

## Folder Structure

The workspace contains two folders by default, where:


src: It contain the Implementaion and GUI of Travel Booking Management folder.

lib: It contain Java-sql-connector.jar to connect to database and TravelBookingManagement.jar file Which is our defined framework.

Role:
Kalpana - Project Lead (Singleton)
Sonam Choden - Project Manager(Observer, State, Template)
Sagar Lepcha - Backend Developer(Proxy, Chain of Responsibility, Strategy)
Jigme Wangyel Wangchuk - Backend Developer(Factory Method)

## Dependency Management

The `JAVA PROJECTS` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-dependency#manage-dependencies).



NEED TO MAKE ADMIN FORM APP.JAVA
NEED TO MAKE STATES
NEED TO MAKE OBSERVER

