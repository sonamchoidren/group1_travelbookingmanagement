package GUI;

import Travel.TravelBookingManagement.DatabaseProxy;

import implementation.DatabaseManager;
import java.sql.ResultSet;
import java.sql.SQLException;


// // ConcreteAdminGUI extends AdminGUI, which likely provides some abstract methods or defines an interface. 
// ConcreteAdminGUI provides implementations for the abstract methods defined in AdminGUI. 
public class ConcreteAdminGUI extends AdminGUI {

    @Override
    public void processData(ResultSet rs) throws SQLException {
        displayTableData(rs);
    }

    @Override
    public boolean updateState(String email, String startingDestination, String endDestination, double cost, String state) {
        DatabaseProxy dbProxy = new DatabaseManager.DatabaseProxyImpl();
        return dbProxy.updateState(email, startingDestination, endDestination, cost, state);
    }

    public static void main(String[] args) {
        new ConcreteAdminGUI();
    }
}
