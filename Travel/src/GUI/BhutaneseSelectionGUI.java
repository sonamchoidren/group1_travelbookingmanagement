package GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

public class BhutaneseSelectionGUI extends JFrame {
    private JCheckBox bhutaneseCheckBox;
    private JButton nextButton;

    public BhutaneseSelectionGUI() {
        setTitle("Bhutanese Selection");
        setSize(300, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        bhutaneseCheckBox = new JCheckBox("I am Bhutanese");
        nextButton = new JButton("Next");

        panel.add(bhutaneseCheckBox);
        panel.add(nextButton);

        add(panel);
        setLocationRelativeTo(null); // Center the window on the screen
        setVisible(true);
    }

    public void setNextButtonListener(ActionListener listener) {
        nextButton.addActionListener(listener);
    }

    public boolean isBhutaneseSelected() {
        return bhutaneseCheckBox.isSelected();
    }
}
