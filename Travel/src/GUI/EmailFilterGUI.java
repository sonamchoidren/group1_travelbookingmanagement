package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.*;
import java.util.Vector;

public class EmailFilterGUI extends JFrame {
    private JTable dataTable;
    private JScrollPane scrollPane;

    public EmailFilterGUI(String email) {
        setTitle("Filter by Email");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5, 5, 5, 5);

        JLabel welcomeLabel = new JLabel("Welcome, displaying data for: " + email);

        dataTable = new JTable();
        scrollPane = new JScrollPane(dataTable);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 3;
        panel.add(welcomeLabel, constraints);

        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        panel.add(scrollPane, constraints);

        add(panel);

        // Center the window on the screen
        setLocationRelativeTo(null);

        // Make the window visible initially
        setVisible(true);

        // Fetch and display data for the provided email
        fetchAndDisplayData(email);
    }

    // Method to execute SQL query with email filter
    public ResultSet executeQuery(String email) throws SQLException {
        String jdbcURL = "jdbc:mysql://localhost:3306/travel";
        String username = "root"; // Replace with your actual username
        String password = ""; // Replace with your actual password

        Connection connection = DriverManager.getConnection(jdbcURL, username, password);
        String query = "SELECT * FROM state WHERE email = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, email);
        return preparedStatement.executeQuery();
    }

    // Method to fetch and display data for the provided email
    private void fetchAndDisplayData(String email) {
        try {
            ResultSet rs = executeQuery(email);
            displayTableData(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle SQL exception
        }
    }

    // Method to display data in the table
    public void displayTableData(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();

        // Column names
        Vector<String> columnNames = new Vector<>();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // Data rows
        Vector<Vector<Object>> data = new Vector<>();
        while (rs.next()) {
            Vector<Object> row = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                row.add(rs.getObject(columnIndex));
            }
            data.add(row);
        }

        // Update the table model
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        dataTable.setModel(model);
    }
}
