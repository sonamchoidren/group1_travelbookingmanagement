package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.sql.*;

// The AdminGUI class is defined as abstract because it provides a 
// general structure and common methods for the admin GUI, but it leaves 
// the implementation of certain key methods to its subclasses.

//Abstract Class (AdminGUI)
public abstract class AdminGUI extends JFrame {
    private JButton viewUsersButton;
    private JButton viewTransactionsButton;
    private JButton viewDiscountButton;
    private JTable dataTable;
    private JScrollPane scrollPane;
    private JButton openDiscountFormButton; // New button for opening the discount form
    // Database proxy variable

    public AdminGUI() {
        
        // Initialize database proxy

        setTitle("Admin Dashboard");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5, 5, 5, 5);

        viewUsersButton = new JButton("View Users");
        viewTransactionsButton = new JButton("View Transactions");
        viewDiscountButton = new JButton("View Discount");

        dataTable = new JTable();
        scrollPane = new JScrollPane(dataTable);

        // Initialize the button
        openDiscountFormButton = new JButton("Open Discount Form");
        constraints.gridx = 3; // Adjust the grid position for the new button
        constraints.gridy = 0;
        panel.add(openDiscountFormButton, constraints);

        // Add action listener to the button
        openDiscountFormButton.addActionListener(e -> openDiscountForm());

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(viewUsersButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 0;
        panel.add(viewTransactionsButton, constraints);

        constraints.gridx = 2;
        constraints.gridy = 0;
        panel.add(viewDiscountButton, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 3;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        panel.add(scrollPane, constraints);

        add(panel);

        // Center the window on the screen
        setLocationRelativeTo(null);

        // Make the window visible initially
        setVisible(true);

        // Register button listeners
        setViewUsersButtonListener(e -> onViewUsers());
        setViewTransactionsButtonListener(e -> onViewTransactions());
        setViewDiscountButtonListener(e -> onViewDiscount());
    }

    // Template method for executing SQL queries
    public ResultSet executeQuery(String query) throws SQLException {
        String jdbcURL = "jdbc:mysql://localhost:3306/travel";
        String username = "root"; // Replace with your actual username
        String password = ""; // Replace with your actual password

        Connection connection = DriverManager.getConnection(jdbcURL, username, password);
        Statement statement = connection.createStatement();
        return statement.executeQuery(query);
    }

    // Template Method:
    // The executeQuery method is a template method. .
    // The abstract method processData(ResultSet rs) is intended to be implemented by subclasses to define 
    // how the result set should be processed and displayed.
    // The displayTableData(ResultSet rs) method is a concrete method that uses the result set to
    //  populate a table. 
    // Abstract method to be implemented by subclasses to process the result set

    //Abstract Method (processData): 
    public abstract void processData(ResultSet rs) throws SQLException;

    // Button action methods
    private void onViewUsers() {
        try {
            ResultSet rs = executeQuery("SELECT * FROM useraccounts");
            processData(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle SQL exception
        }
    }

    private void onViewTransactions() {
        try {
            ResultSet rs = executeQuery("SELECT * FROM state");
            processData(rs);
        } catch (SQLException e) {
        e.printStackTrace();
            // Handle SQL exception
        }
    }

    private void onViewDiscount() {
        try {
            ResultSet rs = executeQuery("SELECT * FROM discount");
            processData(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle SQL exception
        }
    }

    // Button listener setters
    public void setViewUsersButtonListener(ActionListener listener) {
        viewUsersButton.addActionListener(listener);
    }

    public void setViewTransactionsButtonListener(ActionListener listener) {
        viewTransactionsButton.addActionListener(listener);
    }

    public void setViewDiscountButtonListener(ActionListener listener) {
        viewDiscountButton.addActionListener(listener);
    }

    // Method to display data in the table
    public void displayTableData(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();

        // Column names
        Object[] columnNames = new Object[columnCount + 1];
        for (int column = 1; column <= columnCount; column++) {
            columnNames[column - 1] = metaData.getColumnName(column);
        }
        columnNames[columnCount] = "Action";

        // Data rows
        CustomTableModel tableModel = new CustomTableModel(columnNames, 0);
        while (rs.next()) {
            Object[] row = new Object[columnCount + 1];
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                row[columnIndex - 1] = rs.getObject(columnIndex);
            }
            row[columnCount] = "---"; // Placeholder for buttons
            tableModel.addRow(row);
        }

        dataTable.setModel(tableModel);

        if (tableModel.getRowCount() == 11) {
            // Set the renderer and editor only if there are rows in the table
            dataTable.getColumn("Action").setCellRenderer(new ButtonRendererEditor(this));
            dataTable.getColumn("Action").setCellEditor(new ButtonRendererEditor(this));
        }
    }

    public abstract boolean updateState(String email, String startingDestination, String endDestination, double cost, String state);

    // Method to open the discount form
    private void openDiscountForm() {
        // Create an instance of DiscountFormGUI
        DiscountFormGUI discountForm = new DiscountFormGUI();
        // Set the position of the form relative to this AdminGUI
        discountForm.setLocationRelativeTo(this);
    }
}
