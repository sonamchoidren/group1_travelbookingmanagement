package GUI;

import Travel.TravelBookingManagement.DatabaseProxy;

import implementation.DatabaseManager;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class DiscountFormGUI extends JFrame {
    private JTextField discountField;
    private JTextField messageField;
    private JButton submitButton;


    // Concrete Observer: Classes like TravelAppGUI and DiscountFormGUI implement the
    //  DiscountObserver interface, making them concrete observers. They register themselves with the subject 
    //  (RealDatabase) to receive notifications and update their displays accordingly.


    // Implement the Concrete Observers
    public DiscountFormGUI() {
        setTitle("Discount Form");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2, 10, 10));
        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        JLabel discountLabel = new JLabel("Discount:");
        discountField = new JTextField();
        JLabel messageLabel = new JLabel("Message:");
        messageField = new JTextField();

        submitButton = new JButton("Submit");

        panel.add(discountLabel);
        panel.add(discountField);
        panel.add(messageLabel);
        panel.add(messageField);
        panel.add(new JLabel()); // Empty label for spacing
        panel.add(submitButton);

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String discount = discountField.getText();
                String message = messageField.getText();
                try {
                    // Access DatabaseManager directly here
                    DatabaseProxy databaseProxy = DatabaseManager.RealDatabase.getInstance();
                    boolean updated = databaseProxy.updateDiscount(discount, message);
                    if (updated) {
                        JOptionPane.showMessageDialog(DiscountFormGUI.this, "Discount message updated successfully.");
                        dispose(); // Close the form after successful update
                    } else {
                        JOptionPane.showMessageDialog(DiscountFormGUI.this, "Failed to update discount message.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(DiscountFormGUI.this, "An error occurred while updating discount message.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        setLocationRelativeTo(null);
        add(panel);
        setVisible(true);
    }
}
