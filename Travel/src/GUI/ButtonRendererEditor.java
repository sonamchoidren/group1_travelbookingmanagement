package GUI;

import Travel.TravelBookingManagement.BookingState;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import implementation.ConfirmedState;
import implementation.PendingState;
import implementation.RejectedState;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;


public class ButtonRendererEditor extends AbstractCellEditor implements TableCellRenderer, TableCellEditor {
    private JPanel panel;
    private JButton confirmButton;
    private JButton rejectButton;
    private BookingState state;
    private AdminGUI adminGUI;
    private String email;
    private String startingDestination;
    private String endDestination;
    private double cost;

    public ButtonRendererEditor(AdminGUI adminGUI) {
        this.adminGUI = adminGUI;
        panel = new JPanel(new GridLayout(1, 2)); // Ensure buttons are side by side

        confirmButton = new JButton("Confirm");
        rejectButton = new JButton("Reject");

        confirmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                state = new ConfirmedState(adminGUI, email, startingDestination, endDestination, cost);
                state.updateState();
                fireEditingStopped();
            }
        });

        rejectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                state = new RejectedState(adminGUI, email, startingDestination, endDestination, cost);
                state.updateState();
                fireEditingStopped();
            }
        });

        panel.add(confirmButton);
        panel.add(rejectButton);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return panel; // Return panel for rendering
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        email = (String) table.getValueAt(row, 0); // Assuming email is the first column
        startingDestination = (String) table.getValueAt(row, 1); // Assuming starting destination is the second column
        endDestination = (String) table.getValueAt(row, 2); // Assuming end destination is the third column
        Object costValue = table.getValueAt(row, 3); // Assuming cost is the fourth column

        if (costValue instanceof BigDecimal) {
            cost = ((BigDecimal) costValue).doubleValue();
        } else if (costValue instanceof Double) {
            cost = (Double) costValue;
        } else {
            throw new IllegalArgumentException("Unsupported value type for cost: " + costValue.getClass().getName());
        }

        state = new PendingState(adminGUI, email, startingDestination, endDestination, cost);
        return panel; // Return panel for editing
    }

    @Override
    public Object getCellEditorValue() {
        return state;
    }
}
