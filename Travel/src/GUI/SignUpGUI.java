package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

public class SignUpGUI extends JFrame {
    private JTextField usernameField;
    private JTextField emailField;
    private JPasswordField passwordField;
    private JPasswordField confirmPasswordField;
    private JButton signUpButton;
    private JLabel loginLink; // Link to navigate to login GUI

    public SignUpGUI() {
        setTitle("User Details Entry");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.fill = GridBagConstraints.HORIZONTAL;

        usernameField = new JTextField(20);
        emailField = new JTextField(20);
        passwordField = new JPasswordField(20);
        confirmPasswordField = new JPasswordField(20);
        signUpButton = new JButton("Sign Up");
        loginLink = new JLabel("Already have an account? Click here to login.");
        loginLink.setForeground(Color.BLUE);
        loginLink.setCursor(new Cursor(Cursor.HAND_CURSOR)); // Change cursor to hand when hovered

        // Add components to the panel using GridBagConstraints
        constraints.gridx = 0;
        constraints.gridy = 0;
        panel.add(new JLabel("Username:"), constraints);

        constraints.gridx = 1;
        panel.add(usernameField, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        panel.add(new JLabel("Email:"), constraints);

        constraints.gridx = 1;
        panel.add(emailField, constraints);

        constraints.gridx = 0;
        constraints.gridy = 2;
        panel.add(new JLabel("Password:"), constraints);

        constraints.gridx = 1;
        panel.add(passwordField, constraints);

        constraints.gridx = 0;
        constraints.gridy = 3;
        panel.add(new JLabel("Confirm Password:"), constraints);

        constraints.gridx = 1;
        panel.add(confirmPasswordField, constraints);

        constraints.gridx = 1;
        constraints.gridy = 4;
        constraints.anchor = GridBagConstraints.CENTER;
        panel.add(signUpButton, constraints);

        constraints.gridy = 5;
        constraints.gridwidth = 2;
        panel.add(loginLink, constraints);

        add(panel);

        // Center the window on the screen
        setLocationRelativeTo(null);

        setVisible(false); // Initially not visible

        addTooltips(); // Add tooltips to the fields
    }

    private void addTooltips() {
        usernameField.setToolTipText("Enter your desired username");
        emailField.setToolTipText("Enter a valid email address");
        passwordField.setToolTipText("Enter your password (at least 8 characters)");
        confirmPasswordField.setToolTipText("Re-enter your password");
    }

    public void setSignUpButtonListener(ActionListener listener) {
        signUpButton.addActionListener(listener);
    };

    public void setLoginLinkListener(ActionListener listener) {
        loginLink.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listener.actionPerformed(null);
            }
        });
    }

    public String getUsername() {
        return usernameField.getText();
    }

    public String getEmail() {
        return emailField.getText();
    }

    public String getPassword() {
        return new String(passwordField.getPassword());
    }

    public String getConfirmPassword() {
        return new String(confirmPasswordField.getPassword());
    }

    public boolean validateFields() {
        if (getUsername().isEmpty() || getEmail().isEmpty() || getPassword().isEmpty() || getConfirmPassword().isEmpty()) {
            showError("All fields must be filled.");
            return false;
        }

        if (!isValidEmail(getEmail())) {
            showError("Please enter a valid email address.");
            return false;
        }

        if (!getPassword().equals(getConfirmPassword())) {
            showError("Passwords do not match.");
            return false;
        }

        if (getPassword().length() < 8) {
            showError("Password must be at least 8 characters long.");
            return false;
        }

        return true;
    }

    private boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }

    private void showError(String message) {
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
