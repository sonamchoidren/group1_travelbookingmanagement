package GUI;

import Travel.TravelBookingManagement.ChargeCalculationStrategy;
import Travel.TravelBookingManagement.TravelMode;
import Travel.TravelBookingManagement.DatabaseProxy;
import implementation.BusChargeCalculationStrategy;
import implementation.DatabaseManager;
import implementation.FlightChargeCalculationStrategy;
import implementation.TaxiChargeCalculationStrategy;
import implementation.TravelModeFactory;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class TravelAppGUI extends JFrame {
    private JComboBox<String> startDestinationComboBox;
    private JComboBox<String> endDestinationComboBox;
    private JComboBox<String> travelModeComboBox;
    private JTextField  discountField;
    private JTextField emailField;
    private JButton submitButton;
    private JButton ordersButton;
    private String discountMessage;

    public TravelAppGUI(TravelModeFactory modeFactory, String email, DatabaseProxy databaseProxy) {
        setTitle("Travel App");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(7, 2, 10, 10)); // Updated to 7 rows
        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        // Labels
        JLabel discountLabel = new JLabel("Discount Message:");
        discountLabel.setFont(new Font("Arial", Font.PLAIN, 14));
//observer concrete
        // Discount Message Field
    discountField = new JTextField();
    discountField.setFont(new Font("Arial", Font.PLAIN, 14));
    try {
        discountMessage = databaseProxy.getDiscount(); // Get the discount message from the database
    } catch (SQLException ex) {
        ex.printStackTrace();
        // Handle SQL exception
    }
    discountField.setText(discountMessage); // Set the discount message
    discountField.setEditable(false); // Make the discount field non-editable
    panel.add(discountLabel);
    panel.add(discountField);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        JLabel startLabel = new JLabel("Start Destination:");
        startLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        JLabel endLabel = new JLabel("End Destination:");
        endLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        JLabel modeLabel = new JLabel("Travel Mode:");
        modeLabel.setFont(new Font("Arial", Font.PLAIN, 14));

        // Email Field
        emailField = new JTextField();
        emailField.setFont(new Font("Arial", Font.PLAIN, 14));
        emailField.setText(email); // Set the email
        emailField.setEditable(false); // Make the email field non-editable
        panel.add(emailLabel);
        panel.add(emailField);

        // Start Destination Dropdown
        startDestinationComboBox = new JComboBox<>(new String[]{"Chhukha", "Haa", "Paro", "Thimphu", "Gasa", "Punakha", "Tsirang", "Wangdue Phodrang", "Bumthang", "Trongsa"});
        startDestinationComboBox.setFont(new Font("Arial", Font.PLAIN, 14));
        panel.add(startLabel);
        panel.add(startDestinationComboBox);

        // End Destination Dropdown
        endDestinationComboBox = new JComboBox<>(new String[]{"Chhukha", "Haa", "Paro", "Thimphu", "Gasa", "Punakha", "Tsirang", "Wangdue Phodrang", "Bumthang", "Trongsa"});
        endDestinationComboBox.setFont(new Font("Arial", Font.PLAIN, 14));
        panel.add(endLabel);
        panel.add(endDestinationComboBox);

        // Travel Mode Dropdown
        travelModeComboBox = new JComboBox<>(new String[]{"Flight", "Bus", "Taxi"});
        travelModeComboBox.setFont(new Font("Arial", Font.PLAIN, 14));
        panel.add(modeLabel);
        panel.add(travelModeComboBox);

        // Submit Button
        submitButton = new JButton("Submit");
        submitButton.setFont(new Font("Arial", Font.PLAIN, 14));
        panel.add(new JLabel()); // Empty label for spacing
        panel.add(submitButton);

        // Orders Button
        ordersButton = new JButton("Orders");
        ordersButton.setFont(new Font("Arial", Font.PLAIN, 14));
        panel.add(new JLabel()); // Empty label for spacing
        panel.add(ordersButton);

        // Submit Button Action Listener
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String startDestination = (String) startDestinationComboBox.getSelectedItem();
                String endDestination = (String) endDestinationComboBox.getSelectedItem();
                String modeType = (String) travelModeComboBox.getSelectedItem();
                String userEmail = emailField.getText();

                // Create the travel mode based on the selected type
                TravelMode mode = modeFactory.createMode(modeType);

                if (mode != null) {
                    double charge;
                    ChargeCalculationStrategy chargeCalculationStrategy;

                    // Choose the appropriate charge calculation strategy based on the mode type
                    switch (modeType) {
                        case "Taxi":
                            chargeCalculationStrategy = new TaxiChargeCalculationStrategy(startDestination, endDestination);
                            break;
                        case "Flight":
                            chargeCalculationStrategy = new FlightChargeCalculationStrategy(startDestination, endDestination);
                            break;
                        case "Bus":
                            chargeCalculationStrategy = new BusChargeCalculationStrategy(startDestination, endDestination);
                            break;
                        default:
                            throw new IllegalArgumentException("Invalid travel mode selected.");
                    }

                    charge = chargeCalculationStrategy.calculateCharge();

                    // Display charge information
                    JOptionPane.showMessageDialog(TravelAppGUI.this, "Start Destination: " + startDestination + "\nEnd Destination: " + endDestination + "\nTravel Mode: " + modeType + "\nCharge: $" + charge + "\nEmail: " + userEmail, "Travel Details", JOptionPane.INFORMATION_MESSAGE);

                    // Call createState function to insert data into the state table
                    boolean stateCreated = databaseProxy.createState(userEmail, startDestination, endDestination, charge, "pending");

                    if (stateCreated) {
                        System.out.println("State created successfully.");
                        // Launch EmailFilterGUI
                        new EmailFilterGUI(userEmail);
                        TravelAppGUI.this.dispose(); // Close the current window
                    } else {
                        System.out.println("Failed to create state.");
                    }
                } else {
                    JOptionPane.showMessageDialog(TravelAppGUI.this, "Invalid travel mode selected.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        // Orders Button Action Listener
        ordersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Get the user email
                String userEmail = emailField.getText();
                // Launch EmailFilterGUI
                new EmailFilterGUI(userEmail);
                TravelAppGUI.this.dispose(); // Close the current window
            }
        });

        // Center the window on the screen
        setLocationRelativeTo(null);

        add(panel);
        setVisible(true);
    }

    public TravelAppGUI(TravelModeFactory travelModeFactory, String email, Object object) {
     
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Please provide an email address as an argument.");
            System.exit(1);
        }

        String email = args[0]; // Get the email from the command line arguments
        TravelModeFactory modeFactory = new TravelModeFactory();
        DatabaseProxy databaseProxy = new DatabaseManager().new DatabaseProxy();
        new TravelAppGUI(modeFactory, email, databaseProxy);
    }
}