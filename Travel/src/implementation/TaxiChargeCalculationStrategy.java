package implementation;
import Travel.TravelBookingManagement.ChargeCalculationStrategy;

public class TaxiChargeCalculationStrategy implements ChargeCalculationStrategy {
    private String startLocation;
    private String endLocation;

    public TaxiChargeCalculationStrategy(String startLocation, String endLocation) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
    }

    @Override
    public double calculateCharge() {
        Taxi taxi = new Taxi();
        double chargePerKilometer = taxi.getCharge();
        double distance = DatabaseManager.RealDatabase.getInstance().getDistance(startLocation, endLocation);
        
        
        return chargePerKilometer * distance;
    }
}

