package implementation;

import GUI.BhutaneseSelectionGUI;
import GUI.LoginGUI;
import GUI.SignUpGUI;
import GUI.TravelAppGUI;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Travel.TravelBookingManagement.DatabaseProxy;
import Travel.TravelBookingManagement.ValidationHandler;

public class App {
    public static void main(String[] args) {
        // Register MySQL JDBC Driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println("MySQL JDBC Driver not found.");
            e.printStackTrace();
            return;
        }

        BhutaneseSelectionGUI bhutaneseGUI = new BhutaneseSelectionGUI();
        SignUpGUI userDetailsGUI = new SignUpGUI();
        LoginGUI loginGUI = new LoginGUI();
        
        // Create an instance of DatabaseProxy
        DatabaseProxy databaseProxy = new DatabaseManager.DatabaseProxyImpl();
        
        // Create an instance of TravelModeFactory
        TravelModeFactory modeFactory = new TravelModeFactory();

        // Create validation handlers
        ValidationHandler emailHandler = new EmailValidationHandler(); //..
        ValidationHandler passwordMatchHandler = new PasswordMatchValidationHandler();
        ValidationHandler emptyFieldHandler = new EmptyFieldValidationHandler();

        // Chain the validation handlers
        emailHandler.setNextHandler(passwordMatchHandler); 
        passwordMatchHandler.setNextHandler(emptyFieldHandler);

        // ActionListener for Next button on Bhutanese Selection GUI
        bhutaneseGUI.setNextButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Check if Bhutanese is selected
                if (bhutaneseGUI.isBhutaneseSelected()) {
                    // Proceed to UserDetailsEntryGUI
                    bhutaneseGUI.setVisible(false);
                    userDetailsGUI.setVisible(true);
                } else {
                    // Display message to select Bhutanese nationality
                    JOptionPane.showMessageDialog(bhutaneseGUI, "Please select Bhutanese nationality to proceed.");
                }
            }
        });

        // ActionListener for login button on Login GUI
        loginGUI.setLoginButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Get email and password from the login GUI
                String email = loginGUI.getEmail();
                String password = loginGUI.getPassword();

                // Use the proxy to validate the login
                boolean isValidUser = databaseProxy.validateUser(email, password);

                if (isValidUser) {
                    // Login successful
                    int response = JOptionPane.showConfirmDialog(loginGUI, "Login successful!", "Success", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE);

                    // Check if the user clicked "OK"
                    if (response == JOptionPane.OK_OPTION) {
                        // Navigate to the TravelAppGUI with the email
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                loginGUI.setVisible(false);
                                // Create TravelAppGUI with modeFactory and databaseProxy
                                new TravelAppGUI(modeFactory, email, databaseProxy);
                            }
                        });
                    }
                } else {
                    // Invalid email or password
                    JOptionPane.showMessageDialog(loginGUI, "Invalid email or password. Please try again.");
                }
            }
        });

        // ActionListener for login link on UserDetailsEntryGUI to go back to LoginGUI
        userDetailsGUI.setLoginLinkListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userDetailsGUI.setVisible(false);
                loginGUI.setVisible(true);
            }
        });

        // ActionListener for sign-up link on LoginGUI to go to UserDetailsEntryGUI
        loginGUI.setSignUpLinkListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Navigate to sign-up screen
               
                userDetailsGUI.setVisible(true);
                bhutaneseGUI.setVisible(true);
                loginGUI.setVisible(false);
            }
        });

        // ActionListener for Sign Up button on UserDetailsEntryGUI
        userDetailsGUI.setSignUpButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Get user details from UserDetailsEntryGUI
                String username = userDetailsGUI.getUsername();
                String email = userDetailsGUI.getEmail();
                String password = userDetailsGUI.getPassword();
                String confirmPassword = userDetailsGUI.getConfirmPassword();

                // Handle validation using the chain of responsibility
                int value = emailHandler.handleValidationRequest(username, email, password, confirmPassword);

                // If validation passes, proceed with database insertion
                if (value == 1) {
                    boolean isSignedUp = databaseProxy.registerUser(email, username, password);
                    if (isSignedUp) {
                        JOptionPane.showMessageDialog(userDetailsGUI, "User signed up successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(userDetailsGUI, "Failed to sign up user.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        // Initialize the GUI
        bhutaneseGUI.setVisible(false);
    }
}
