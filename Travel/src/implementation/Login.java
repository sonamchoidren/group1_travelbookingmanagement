package implementation;
import GUI.*;

import Travel.TravelBookingManagement.DatabaseProxy;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login {
    public static void main(String[] args) {
        // Create instances of the GUIs
        SignUpGUI signUpGUI = new SignUpGUI();
        LoginGUI loginGUI = new LoginGUI();

        // Set up the action listener for the login button
        loginGUI.setLoginButtonListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                // Get email and password from the login GUI
                String email = loginGUI.getEmail();
                String password = loginGUI.getPassword();

                DatabaseProxy dbProxy = new DatabaseManager.DatabaseProxyImpl();
                if (dbProxy.validateUser(email, password)) {
                    // Login successful
                    JOptionPane.showMessageDialog(loginGUI, "Login successful!");
                    // Navigate to the TravelAppGUI with the email
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            loginGUI.setVisible(false);
                            new TravelAppGUI(new TravelModeFactory(), email, null);
                        }
                    });
                } else {
                    // Invalid email or password
                    JOptionPane.showMessageDialog(loginGUI, "Invalid email or password. Please try again.");
                }
            }
        });

        signUpGUI.setLoginLinkListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signUpGUI.setVisible(false);
                loginGUI.setVisible(true);
            }
        });

        // Start with the login GUI
        loginGUI.setVisible(true);
    }
}
