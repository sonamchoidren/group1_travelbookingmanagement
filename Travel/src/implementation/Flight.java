package implementation;
import Travel.TravelBookingManagement.TravelMode;

public class Flight implements TravelMode {
    private static final double CHARGE = 3.9;
    private static final String NAME = "Flight";
    private static final double service = 500;



    public double getservice() {
        return service;
    }
    @Override
    public double getCharge() {
        return CHARGE;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
