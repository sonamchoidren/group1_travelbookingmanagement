package implementation;
import Travel.TravelBookingManagement.BookingState;

import GUI.AdminGUI;

public class PendingState extends BookingState {

    public PendingState(AdminGUI adminGUI, String email, String startingDestination, String endDestination, double cost) {
        super(adminGUI, email, startingDestination, endDestination, cost);
    }

    @Override
    public void updateState() {
        // Pending state doesn't require any update
    }
}
