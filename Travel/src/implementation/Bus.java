package implementation;
import Travel.TravelBookingManagement.TravelMode;

public class Bus implements TravelMode {
    private static final double CHARGE = 1.2;
    private static final String NAME = "Bus";

    @Override
    public double getCharge() {
        return CHARGE;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
