package implementation;
import Travel.TravelBookingManagement.BookingState;

import GUI.AdminGUI;
import implementation.RejectedState;

import javax.swing.JOptionPane;

public class RejectedState extends BookingState {

    public RejectedState(AdminGUI adminGUI, String email, String startingDestination, String endDestination, double cost) {
        super(adminGUI, email, startingDestination, endDestination, cost);
    }

    @Override
    public void updateState() {
        boolean success = adminGUI.updateState(email, startingDestination, endDestination, cost, "Rejected");
        if (!success) {
            JOptionPane.showMessageDialog(adminGUI, "Failed to update state.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(adminGUI, "State updated successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
