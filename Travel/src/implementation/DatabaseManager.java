package implementation;

import Travel.TravelBookingManagement.DatabaseProxy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseManager {

    // Real database class implementing the interface
    //  concreate Subject: RealDatabase class (manages the state and notifies USER)

    public static class RealDatabase implements DatabaseProxy {
        private static RealDatabase instance;
        private String url = "jdbc:mysql://localhost:3306/travel";
        private String dbUsername = "root"; // Replace with your actual username
        private String dbPassword = "";

        // Private constructor to prevent instantiation from outside
        private RealDatabase() {}

        // Static method to get the singleton instance
        public static RealDatabase getInstance() {
            if (instance == null) {
                synchronized (RealDatabase.class) {
                    if (instance == null) {
                        instance = new RealDatabase();
                    }
                }
            }
            return instance;
        }

        @Override
        public boolean validateUser(String email, String password) {
            String query = "SELECT * FROM useraccounts WHERE email = ? AND password = ?";
            try (Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, email);
                statement.setString(2, password);
                ResultSet resultSet = statement.executeQuery();
                return resultSet.next();
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        }

        @Override
        public String getDiscount() throws SQLException {
            String discountMessage = "";
            String query = "SELECT message FROM discountmessages";
            try (Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement statement = connection.prepareStatement(query);
                 ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    discountMessage = resultSet.getString("message");
                }
            }
            return discountMessage;
        }

        @Override
        public boolean createState(String email, String startingDestination, String endDestination, double cost, String state) {
            String query = "INSERT INTO state (email, starting_destination, end_destination, cost, state) VALUES (?, ?, ?, ?, ?)";
            try (Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, email);
                statement.setString(2, startingDestination);
                statement.setString(3, endDestination);
                statement.setDouble(4, cost);
                statement.setString(5, state);
                int rowsInserted = statement.executeUpdate();
                return rowsInserted > 0;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        }
    

        @Override
        public boolean registerUser(String email, String username, String password) {
            String query = "INSERT INTO useraccounts (email, username, password) VALUES (?, ?, ?)";
            try (Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, email);
                statement.setString(2, username);
                statement.setString(3, password);
                int rowsInserted = statement.executeUpdate();
                return rowsInserted > 0;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        }

        @Override
        public double getDistance(String starting_destination, String end_destination) {
            double distance = 0.0;
            String query = "SELECT distance_km FROM distance WHERE starting_destination = ? AND end_destination = ?";

            try (Connection conn = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement stmt = conn.prepareStatement(query)) {

                stmt.setString(1, starting_destination);
                stmt.setString(2, end_destination);

                ResultSet rs = stmt.executeQuery();

                if (rs.next()) {
                    distance = rs.getDouble("distance_km");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return distance;
        }

        @Override
        public boolean updateState(String email, String startingDestination, String endDestination, double cost, String state) {
            String query = "UPDATE state SET starting_destination = ?, end_destination = ?, cost = ?, state = ? WHERE email = ?";
            try (Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, startingDestination);
                statement.setString(2, endDestination);
                statement.setDouble(3, cost);
                statement.setString(4, state);
                statement.setString(5, email);
                int rowsUpdated = statement.executeUpdate();
                return rowsUpdated > 0;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        }


        @Override
        public boolean updateDiscount(String discount, String message) throws SQLException {
            String query = "UPDATE discountmessages SET discount = ?, message = ?";
            try (Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, discount);
                statement.setString(2, message);
                int rowsUpdated = statement.executeUpdate();
                return rowsUpdated > 0;
            }
        }

        @Override
        public ResultSet executeQuery(String string) {
            throw new UnsupportedOperationException("Unimplemented method 'executeQuery'");
        }
    }

    // Proxy class controlling access to the real database
    // Proxy (DatabaseProxyImpl)
    public static class DatabaseProxyImpl implements DatabaseProxy {
        private RealDatabase realDatabase;

        @Override
        public boolean validateUser(String email, String password) {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.validateUser(email, password);// ..
        }


        @Override
        public String getDiscount() throws SQLException {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.getDiscount();
        }

        @Override
        public boolean registerUser(String email, String username, String password) {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.registerUser(email, username, password);
        }

        @Override
        public double getDistance(String startLocation, String endLocation) {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.getDistance(startLocation, endLocation);
        }

        @Override
        public boolean createState(String email, String startingDestination, String endDestination, double cost, String state) {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.createState(email, startingDestination, endDestination, cost, state);
        }

        @Override
        public boolean updateState(String email, String startingDestination, String endDestination, double cost, String state) {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.updateState(email, startingDestination, endDestination, cost, state);
        }


        @Override
        //notifyObservers
        public boolean updateDiscount(String discount, String message) throws SQLException {
            if (realDatabase == null) {
                realDatabase = RealDatabase.getInstance();
            }
            return realDatabase.updateDiscount(discount, message);
        }

        
        @Override
        public ResultSet executeQuery(String string) {
            throw new UnsupportedOperationException("Unimplemented method 'executeQuery'");
        }
    }
}


// // the proxy pattern in this context provides controlled access to the RealDatabase instance, 
// ensuring it is created only 
// when necessary and allowing the proxy to add any additional behavior needed for the application.