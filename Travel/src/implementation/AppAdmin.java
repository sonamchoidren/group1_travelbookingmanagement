package implementation;

import javax.swing.JOptionPane;
import GUI.ConcreteAdminGUI;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppAdmin {
    public static void main(String[] args) {
        // Create and display the ConcreteAdminGUI
        ConcreteAdminGUI adminGUI = new ConcreteAdminGUI();

        // Admin GUI Button Listeners
        adminGUI.setViewUsersButtonListener(e -> {
            try {
                ResultSet rs = adminGUI.executeQuery("SELECT * FROM useraccounts");
                adminGUI.processData(rs);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(adminGUI, "Error fetching user data: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        adminGUI.setViewTransactionsButtonListener(e -> {
            try {
                ResultSet rs = adminGUI.executeQuery("SELECT * FROM state");
                adminGUI.processData(rs);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(adminGUI, "Error fetching transaction data: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        adminGUI.setViewDiscountButtonListener(e -> {
            try {
                ResultSet rs = adminGUI.executeQuery("SELECT * FROM discountmessages");
                adminGUI.processData(rs);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(adminGUI, "Error fetching discount data: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        // Display the ConcreteAdminGUI
        adminGUI.setVisible(true);
    }
}
