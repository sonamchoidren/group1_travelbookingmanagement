package implementation;

import Travel.TravelBookingManagement.ChargeCalculationStrategy;

public class BusChargeCalculationStrategy implements ChargeCalculationStrategy {
    private String startLocation;
    private String endLocation;

    public BusChargeCalculationStrategy(String startLocation, String endLocation) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
    }

    @Override
    public double calculateCharge() {
        Bus bus = new Bus();
        double chargePerKilometer = bus.getCharge();
        double distance = DatabaseManager.RealDatabase.getInstance().getDistance(startLocation, endLocation);
        return chargePerKilometer * distance /20;
    }
}
