package implementation;

import Travel.TravelBookingManagement.TravelMode;
//context
public class TravelModeFactory {
    public TravelMode createMode(String modeType) {
        
        if (modeType == null) {
            return null;
        }
        if (modeType.equalsIgnoreCase("Flight")) {
            return new Flight();
        } else if (modeType.equalsIgnoreCase("Bus")) {
            return new Bus();
        } else if (modeType.equalsIgnoreCase("Taxi")) {
            return new Taxi();
        }
        return null;
    }
}
