package implementation;

import Travel.TravelBookingManagement.ChargeCalculationStrategy;

public class FlightChargeCalculationStrategy implements ChargeCalculationStrategy {
    private String startLocation;
    private String endLocation;

    public FlightChargeCalculationStrategy(String startLocation, String endLocation) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
    }

    @Override
    public double calculateCharge() {
        Flight flight = new Flight();
        double chargePerKilometer = flight.getCharge();
        double service = flight.getservice();
        double distance = DatabaseManager.RealDatabase.getInstance().getDistance(startLocation, endLocation);
        return chargePerKilometer * distance + service ;
    }
}
