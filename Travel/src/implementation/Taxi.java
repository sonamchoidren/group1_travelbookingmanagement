package implementation;
import Travel.TravelBookingManagement.TravelMode;

public class Taxi implements TravelMode {
    private static final double CHARGE = 1.8;
    private static final String NAME = "Taxi";

    @Override
    public double getCharge() {
        return CHARGE;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
