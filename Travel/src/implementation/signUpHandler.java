package implementation;

import javax.swing.*;

import Travel.TravelBookingManagement.ValidationHandler;

// Concrete validation handler for checking if the email format is valid
class EmailValidationHandler implements ValidationHandler {
    private ValidationHandler nextHandler;

    @Override
    public void setNextHandler(ValidationHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public int handleValidationRequest(String username, String email, String password, String confirmPassword) {
        if (!isValidEmail(email)) {
            JOptionPane.showMessageDialog(null, "Invalid email address.", "Validation Error", JOptionPane.ERROR_MESSAGE);
            return 0;
        } else if (nextHandler != null) {
            return nextHandler.handleValidationRequest(username, email, password, confirmPassword);
        }
        return 1;
    }

    private boolean isValidEmail(String email) {
        // Regular expression pattern to validate email format
        String emailPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        return email.matches(emailPattern);
    }
}

// Concrete validation handler for checking if the password matches the confirm password
class PasswordMatchValidationHandler implements ValidationHandler {
    private ValidationHandler nextHandler;

    @Override
    public void setNextHandler(ValidationHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public int handleValidationRequest(String username, String email, String password, String confirmPassword) {
        if (!password.equals(confirmPassword)) {
            JOptionPane.showMessageDialog(null, "Passwords do not match.", "Validation Error", JOptionPane.ERROR_MESSAGE);
            return 0;
        } else if (nextHandler != null) {
            return nextHandler.handleValidationRequest(username, email, password, confirmPassword);
        }
        return 1;
    }
}

// Concrete validation handler for checking if the username, email, and password are not empty
class EmptyFieldValidationHandler implements ValidationHandler {
    private ValidationHandler nextHandler;

    @Override
    public void setNextHandler(ValidationHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public int handleValidationRequest(String username, String email, String password, String confirmPassword) {
        if (username.isEmpty() || email.isEmpty() || password.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please fill in all fields.", "Validation Error", JOptionPane.ERROR_MESSAGE);
            return 0;
        } else if (nextHandler != null) {
            return nextHandler.handleValidationRequest(username, email, password, confirmPassword);
        }
        return 1;
    }


    
}



