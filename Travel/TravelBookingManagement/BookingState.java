package Travel.TravelBookingManagement;

import GUI.AdminGUI;

public abstract class BookingState {
    protected AdminGUI adminGUI;
    protected String email;
    protected String startingDestination;
    protected String endDestination;
    protected double cost;

    public BookingState(AdminGUI adminGUI, String email, String startingDestination, String endDestination, double cost) {
        this.adminGUI = adminGUI;
        this.email = email;
        this.startingDestination = startingDestination;
        this.endDestination = endDestination;
        this.cost = cost;
    }

    public abstract void updateState();
}
