package Travel.TravelBookingManagement;

public interface TravelMode {
    double getCharge();
    String getName();
}
