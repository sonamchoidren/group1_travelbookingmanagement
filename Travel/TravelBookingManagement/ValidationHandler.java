package Travel.TravelBookingManagement;

public interface ValidationHandler {
    void setNextHandler(ValidationHandler handler);
    int handleValidationRequest(String username, String email, String password, String confirmPassword);
}

