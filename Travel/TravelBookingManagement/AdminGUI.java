package Travel.TravelBookingManagement;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface AdminGUI {
    void processData(ResultSet rs) throws SQLException;
    boolean updateState(String email, String startingDestination, String endDestination, double cost, String state);
}
