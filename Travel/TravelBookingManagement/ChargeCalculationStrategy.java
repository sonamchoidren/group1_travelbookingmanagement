package Travel.TravelBookingManagement;

public interface ChargeCalculationStrategy {
    double calculateCharge();
}
