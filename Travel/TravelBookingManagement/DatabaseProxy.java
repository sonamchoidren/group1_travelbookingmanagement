package Travel.TravelBookingManagement;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface DatabaseProxy {
        boolean validateUser(String email, String password);
        boolean registerUser(String email, String username, String password);
        double getDistance(String startLocation, String endLocation);
        boolean createState(String email, String startingDestination, String endDestination, double cost, String state);
        boolean updateState(String email, String startingDestination, String endDestination, double cost, String state); 
        boolean updateDiscount(String discount, String message) throws SQLException; // New method
        String getDiscount() throws SQLException;
        ResultSet executeQuery(String string);
    }

    

